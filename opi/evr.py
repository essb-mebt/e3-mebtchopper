#!/usr/bin/python3
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
import epics
import numpy as np
import time
from datetime import datetime
import pyqtgraph as pg
class MainWindow(QtWidgets.QMainWindow):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #super(MainWindow,self).__init__(*args,**kwargs)
        uic.loadUi("myevr.ui", self)
        self.timedelay = self.findChild(QtWidgets.QLineEdit,"lineEdit")
     
        self.timedelay.textEdited.connect(self.WritePV)
        self.commitB = self.findChild(QtWidgets.QPushButton,"pushButton")
        self.commitB.clicked.connect(self.Commit)
        layout = self.findChild(QtWidgets.QVBoxLayout,"verticalLayout")
        self.pw = pg.PlotWidget(title="Chopper EVR")
        self.pw.setLabel('bottom', "T delay", units='s')
        self.pw.setLabel('left',"A.U.")
        self.pw.addLegend(offset=(150,50))
        
        layout.addWidget(self.pw)
        

        
        
        self.curve14A = self.pw.plot([0,0],[0,10],pen='y',name='14 Hz')
        self.curve14B = self.pw.plot(pen='y')
        self.curve15A = self.pw.plot(pen=pg.mkPen('r',width=3),name='TTL A Output')
        self.curve15B = self.pw.plot(pen=pg.mkPen('r',width=3),name='TTL B Output')        
            
        
        #self.timer = QtCore.QTimer()
        #self.timer.timeout.connect(self.myupdate)
        #self.timer.start(0.001)
        
        self.out0delay = epics.PV("MEBT-EVR:P1Width").get()/1.e6
        self.commitPV = epics.PV("MEBT-EVR:Seq0SPRun") 
        self.tdelayPV = epics.PV(self.timedelay.text())
        self.timedelay.setText(self.tdelayPV.char_value)
        self.commRB = epics.PV("MEBT-EVR:SoftSeq0-Commit-RB")
        self.commitB.setText(self.commRB.enum_strs[self.commRB.get()])
        self.evt14 = epics.PV("MEBT-EVR:EvtACnt-I",callback=self.onChanges14)
        self.evt15 = epics.PV("MEBT-EVR:EvtCCnt-I",callback=self.onChanges15)
    
        self.ts14 = []
    def onChanges14(self,**kw):
        self.ts14.append( self.evt14.timestamp )
        if len(self.ts14) == 2:
            dt14 = (self.ts14[1]-self.ts14[0])
            self.ts14 = []
            self.curve14B.setData([dt14,dt14],[0,10])
        

    def onChanges15(self,**kw):
        ts14 = self.evt14.timestamp
        ts15 = self.evt15.timestamp
        self.dt = (ts15-ts14)
        if self.dt < 1e-6:
            self.curve15A.setData([self.dt,self.dt,self.dt+self.out0delay,self.dt+self.out0delay],[0,5,5,0]) 
        else:
            self.curve15B.setData([self.dt,self.dt,self.dt+self.out0delay,self.dt+self.out0delay],[0,5,5,0])
       


    def myupdate(self):
        self.tstamp14 = self.evt14.timestamp
        self.tstamp15 = self.evt15.timestamp
        print (self.tstamp15 - self.tstamp14)


    def WritePV(self):
        self.tdelayPV.put(float(self.timedelay.text()),wait=True)
        self.commitB.setText(self.commRB.enum_strs[self.commRB.get()])
    
    def Commit(self):
        self.commitPV.put(1,wait=True)
        self.commitB.setText(self.commRB.enum_strs[self.commRB.get()])
                                                                                                                                 
                                                                                                                                    
                                                                                                                               

app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
