#!/usr/bin/python
import sys
from pyqtgraph.Qt import QtCore, QtGui
from PySide import QtUiTools
import epics
import numpy as np
import time
from datetime import datetime
import pyqtgraph as pg
class MainWindow(QtCore.QObject):
    
    def __init__(self, *args, **kwargs):
                
        super(MainWindow,self).__init__(*args,**kwargs)
        
         
        loader = QtUiTools.QUiLoader()
        self.win = loader.load('myevr.ui')
        
        self.timedelay = self.win.findChild(QtGui.QLineEdit,"lineEdit") 
        self.timedelay.textEdited.connect(self.WritePV)
        self.commitB = self.win.findChild(QtGui.QPushButton,"pushButton")
        self.commitB.clicked.connect(self.Commit)
        layout = self.win.findChild(QtGui.QVBoxLayout,"verticalLayout")
        self.plot = pg.PlotWidget()

        layout.addWidget(self.plot)
        
                
        self.commitPV = epics.PV("local:myseq") 
        self.tdelayPV = epics.PV(str(self.timedelay.text()))
        self.timedelay.setText(self.tdelayPV.char_value)
        self.commRB = epics.PV("MEBT-EVR:SoftSeq0-Commit-RB")
        self.commitB.setText(self.commRB.enum_strs[self.commRB.get()])
        self.evt14 = epics.PV("MEBT-EVR:EvtACnt-I")
        self.evt15 = epics.PV("MEBT-EVR:EvtCCnt-I")
        self.win.show()

    def WritePV(self):
        self.tdelayPV.put(float(self.timedelay.text()),wait=True)
        self.commitB.setText(self.commRB.enum_strs[self.commRB.get()])
    
    def Commit(self):
        self.commitPV.put(1,wait=True)
        self.commitB.setText(self.commRB.enum_strs[self.commRB.get()])
                                                                                                                                 
                                                                                                                                    
                                                                                                                                    

app = QtGui.QApplication(sys.argv)
window = MainWindow()
app.exec_()
