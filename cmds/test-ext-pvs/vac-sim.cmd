
#- MEBT-010:BMD-Chop-001
epicsEnvSet(SYSSUB,    "MEBT-010")
epicsEnvSet(CHPDEV,    "BMD-Chop-001")
epicsEnvSet(PLCDEV,    "BMD-PLC-012")
epicsEnvSet(PULPOSDEV, "PwrC-PSChop-001P")
epicsEnvSet(PULNEGDEV, "PwrC-PSChop-001N")

#
#- MEBT Chopper Health Status
#
dbLoadRecords("$(E3_CMD_TOP)/vac.template", "P=$(SYSSUB):, R=$(CHPDEV):, PLC=$(PLCPORT=0), INOFF=90, DESC='MEBT Chopper Health (to MPS)'")


#- Start IOC
iocInit()

