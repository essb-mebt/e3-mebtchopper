
require mebtchopper,develop
require s7plc,1.4.1
require sequencer,2.2.7
require autosave,5.10.0
require stream,2.8.10

#- MEBT-010:BMD-Chop-001
epicsEnvSet(SYSTEM,    "MEBT")
epicsEnvSet(SUBSYSTEM, "010")
epicsEnvSet(DISCIPLINE, "BMD")
epicsEnvSet(DEVICE,     "Chop-001")
epicsEnvSet(PREFIX,"$(SYSTEM)-$(SUBSYSTEM):$(DISCIPLINE)-$(DEVICE)")
epicsEnvSet(SYSSUB, "$(SYSTEM)-$(SUBSYSTEM)")

#- Connect to PLC
s7plcConfigure("mebtchplc", "172.30.6.0", 2000, 100, 100, 1, 1000, 1000)
var s7plcDebug 0

#- PLC records loading
epicsEnvSet(PLCDEV,"$(SYSTEM)-$(SUBSYSTEM):BMD-PLC-012:")

#- PLC digital input channels (flow switches)
dbLoadRecords("mebtch_plc_di.template", "P=$(PLCDEV), R=DI0, PLC=mebtchplc, INOFF=0, OUTOFF=0, DESC='Load + FS'")
dbLoadRecords("mebtch_plc_di.template", "P=$(PLCDEV), R=DI1, PLC=mebtchplc, INOFF=2, OUTOFF=2, DESC='Load - FS'")
#dbLoadRecords("mebtch_plc_di.template", "P=$(PLCDEV), R=DI4, PLC=mebtchplc, INOFF=106, OUTOFF=76, DESC='Dump FS'")

#- PLC digital input channel (24V status)
dbLoadRecords("mebtch_plc_di.template", "P=$(PLCDEV), R=DI8, PLC=mebtchplc, INOFF=88, OUTOFF=64, DESC='24VDC OK'")

#- PLC digital output (health status)
dbLoadRecords("mebtch_plc_do.template", "P=$(PLCDEV), R=DO0, PLC=mebtchplc, INOFF=90, DESC=PLCDO0")

#- PLC connection status
dbLoadRecords("mebtch_plc_stat.template", "P=$(PLCDEV), R=, PLC=mebtchplc")

#- PLC analog input (temperatures)
dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI0, PLC=mebtchplc, INOFF=4,  OUTOFF=4,  DESC='Load + Temp'")
dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI1, PLC=mebtchplc, INOFF=18, OUTOFF=14, DESC='Load - Temp'")
dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI4, PLC=mebtchplc, INOFF=32, OUTOFF=24, DESC='Beam Out Bot.'")
dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI5, PLC=mebtchplc, INOFF=46, OUTOFF=34, DESC='Beam Out Top'")
dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI6, PLC=mebtchplc, INOFF=60, OUTOFF=44, DESC='Beam In Top'")
dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI7, PLC=mebtchplc, INOFF=74, OUTOFF=54, DESC='Beam In Bot.'")
#dbLoadRecords("mebtch_plc_ai.template", "P=$(PLCDEV), R=AI2, PLC=mebtchplc, INOFF=92, OUTOFF=66, DESC='Dump Temp'")

#- PLC local protection system variables
dbLoadRecords("mebtch_plc_lps_vars.template", "P=$(PLCDEV), R=, PLC=mebtchplc, OUTOFF=78")

#- State machine control
epicsEnvSet(PULPOSSTAT, "$(PREFIX):PulsersSimulated")
epicsEnvSet(PLCVAR0,    "$(PLCDEV)Var0-Stat-SP")

epicsEnvSet(PULNEGSTAT, "$(PREFIX):PulsersSimulated")
epicsEnvSet(PLCVAR1,    "$(PLCDEV)Var1-Stat-SP")

epicsEnvSet(PLCDOVAR,   "$(PLCDEV)DO0-Stat-Sim-RB")
epicsEnvSet(PLCVAR2,    "$(PLCDEV)Var2-Stat-SP")

dbLoadRecords("mebtch_ctrl_statemac.template", "P=$(PREFIX):, R=, PULPOSSTAT=$(PULPOSSTAT), PLCVAR0=$(PLCVAR0), PULNEGSTAT=$(PULNEGSTAT), PLCVAR1=$(PLCVAR1), PLCDOVAR=$(PLCDOVAR), PLCVAR2=$(PLCVAR2)")

##########################

#- Temperature Sensors
epicsEnvSet(TE1, "$(SYSSUB):BMD-TE-013")
epicsEnvSet(TE2, "$(SYSSUB):BMD-TE-014")
epicsEnvSet(TE3, "$(SYSSUB):BMD-TE-015")
epicsEnvSet(TE4, "$(SYSSUB):BMD-TE-016")
epicsEnvSet(TE5, "$(SYSSUB):BMD-TE-017")
epicsEnvSet(TE6, "$(SYSSUB):BMD-TE-018")
epicsEnvSet(TE7, "$(SYSSUB):BMD-TE-001")

#- Water Flow Switches
epicsEnvSet(FS1, "$(SYSSUB):BMD-FS-012")
epicsEnvSet(FS2, "$(SYSSUB):BMD-FS-013")
epicsEnvSet(FS3, "$(SYSSUB):BMD-FS-014")

#- Temperature Sensors Records
dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE1), R=, PLCAI=$(PLCDEV)AI0, DESC='Load +'")
dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE2), R=, PLCAI=$(PLCDEV)AI1, DESC='Load -'")
dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE3), R=, PLCAI=$(PLCDEV)AI4, DESC='Beam Out Bot.'")
dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE4), R=, PLCAI=$(PLCDEV)AI5, DESC='Beam Out Top'")
dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE5), R=, PLCAI=$(PLCDEV)AI6, DESC='Beam In Top'")
dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE6), R=, PLCAI=$(PLCDEV)AI7, DESC='Beam In Bot.'")
#dbLoadRecords("mebtch_tmpsensor.template", "P=$(TE7), R=, PLCAI=$(PLCDEV)AI2, DESC='Dump'")

#- Flow Switch Records
dbLoadRecords("mebtch_wtrflow.template", "P=$(FS1), R=, PLCDI=$(PLCDEV)DI0, DESC='Load +'")
dbLoadRecords("mebtch_wtrflow.template", "P=$(FS2), R=, PLCDI=$(PLCDEV)DI1, DESC='Load -'")
#dbLoadRecords("mebtch_wtrflow.template", "P=$(FS3), R=, PLCDI=$(PLCDEV)DI2, DESC='Dump'")

#- 24V Selectivity Module
dbLoadRecords("mebtch_24Vmod.template", "P=$(FS2), R=, PLCDI=$(PLCDEV)DI8, DESC='24V Mod'")

#- autosave configuration
epicsEnvSet("TOP", "$(E3_CMD_TOP)")
epicsEnvSet("IOCNAME", "MEBT-010:Ctrl-IOC-001")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(TOP),IOCNAME=$(IOCNAME)")


#############################################################
# Pulsers Control
#############################################################
# Connecting Positive and Negative PS
epicsEnvSet("STREAM_PROTOCOL_PATH","$(mebtchopper_DB)")
epicsEnvSet(POSPULS, "PwrC-PSChop-001P")
epicsEnvSet(NEGPULS, "PwrC-PSChop-001N")

drvAsynIPPortConfigure("$(POSPULS)","127.0.0.1:4001")
drvAsynIPPortConfigure("$(NEGPULS)","127.0.0.1:4002")

#Give Asyn some time to successfully connect
#sleep 1
dbLoadRecords("streamFIDPulser.template", "P=$(SYSSUB):$(POSPULS):, R=Comm, PULSCONN=$(POSPULS)")
dbLoadRecords("streamFIDPulser.template", "P=$(SYSSUB):$(NEGPULS):, R=Comm, PULSCONN=$(NEGPULS)")


iocInit()

