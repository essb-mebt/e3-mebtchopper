#!/usr/bin/env python

import sys
import os
import epics
import multiprocessing
import subprocess
import time

PREF = "MEBT-010:TS-EVR-001"


os.environ["PATH"]+=":/epics/base-7.0.3/bin/linux-x86_64/"



def Create14SE():
    softEVT = epics.PV("%s:EvtCode-SP" % PREF)
    f = 1./14.
    print ("Starting 14 Hz Event Simulator....")
    while True:
        softEVT.put("14")
        time.sleep(f)


t = multiprocessing.Process(target=Create14SE)
t.start()

cmd = "/epics/base-7.0.3/require/3.1.2/bin/iocsh.bash /home/iocuser/Templates/e3-mebt_fastchopper/cmds/evr_mtca_300_ps.cmd"



subprocess.call(cmd, shell=True)


