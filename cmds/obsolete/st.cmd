
require mebtchopper
require s7plc
require sequencer
require autosave
require stream

#- MEBT-010:BMD-Chop-001
epicsEnvSet(SYSSUB,    "MEBT-010")
epicsEnvSet(CHPDEV,    "BMD-Chop-001")
epicsEnvSet(PLCDEV,    "BMD-PLC-012")
epicsEnvSet(PULPOSDEV, "PwrC-PSChop-001P")
epicsEnvSet(PULNEGDEV, "PwrC-PSChop-001N")

epicsEnvSet("STREAM_PROTOCOL_PATH","$(mebtchopper_DB)")


#- Connect to PLC
epicsEnvSet(PLCPORT, "mebtchplc")
s7plcConfigure($(PLCPORT), "172.16.60.159", 2000, 110, 100, 1, 2000, 1000)
var s7plcDebug 0

#---------------------------------- Local Protection System Records ----------------------------------
#- Macros description:
# $(SYSSUB) ----> SYSTEM-SUBSYSTEM
# $(PLCDEV) ----> Discipline-Device name for the PLC device
# $(PLCIO)  ----> Identification of the IO in the PLC IO list (AI0, BI0, etc...)
# $(PORT)   ----> PLC port handler (from s7plcConfigure)
# $(IN/OUTOFF) -> offset address for this IO in the PLC internal database
# $(FLDDEV) ----> Field device name connected to PLC input (flow switch)
# $(DESC) ------> Device description
#-----------------------------------------------------------------------------------------------------

#- PLC connection status
dbLoadRecords("mebtch_plc_stat.template", "P=$(SYSSUB):, R=$(PLCDEV):, PLC=$(PLCPORT)")

#- Positive Load - Flow Switch and Temperature (PT-100)
epicsEnvSet(FS, "BMD-FS-012")
epicsEnvSet(TE, "BMD-TE-013")
iocshLoad("$(mebtchopper_DIR)mebtchopper_wtrflow.iocsh",   "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=DI0, INOFF=0, OUTOFF=0, FLDDEV=$(FS), DESC='Load + Flow'")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI0, INOFF=4, OUTOFF=4, FLDDEV=$(TE), DESC='Load + Temp'")

#- Negative Load - Flow Switch and Temperature (PT-100)
epicsEnvSet(FS, "BMD-FS-013")
epicsEnvSet(TE, "BMD-TE-014")
iocshLoad("$(mebtchopper_DIR)mebtchopper_wtrflow.iocsh",   "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=DI1, INOFF=2,  OUTOFF=2,  FLDDEV=$(FS), DESC='Load - Flow'")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI1, INOFF=18, OUTOFF=14, FLDDEV=$(TE), DESC='Load + Temp'")

#- Beam Dump - Flow Switch and Temperature (PT-100)
epicsEnvSet(FS, "BMD-FS-014")
epicsEnvSet(TE, "BMD-TE-001")
iocshLoad("$(mebtchopper_DIR)mebtchopper_wtrflow.iocsh",   "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=DI4, INOFF=106, OUTOFF=76, FLDDEV=$(FS), DESC='Beam Dump Flow'")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI2, INOFF=92,  OUTOFF=66, FLDDEV=$(TE), DESC='Load + Temp'")

#- 24V Supply Status
epicsEnvSet(FIELD_DEV, "BMD-PS-012")
iocshLoad("$(mebtchopper_DIR)mebtchopper_24vdc_plc.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=DI8, INOFF=88, OUTOFF=64, FLDDEV=$(FIELD_DEV), DESC='Beam Dump Flow'")

#- Choper vessel temperature sensor: Beam Out Bottom 
epicsEnvSet(TE, "BMD-TE-015")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI4, INOFF=32, OUTOFF=24, FLDDEV=$(TE), DESC='Beam Out Bot.'")

#- Choper vessel temperature sensor: Beam Out Top 
epicsEnvSet(TE, "BMD-TE-016")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI5, INOFF=46, OUTOFF=34, FLDDEV=$(TE), DESC='Beam Out Top'")

#- Choper vessel temperature sensor: Beam In Top 
epicsEnvSet(TE, "BMD-TE-017")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI6, INOFF=60, OUTOFF=44, FLDDEV=$(TE), DESC='Beam In Top'")

#- Choper vessel temperature sensor: Beam In Bottom 
epicsEnvSet(TE, "BMD-TE-018")
iocshLoad("$(mebtchopper_DIR)mebtchopper_tmpsensor.iocsh", "SYSSUB=$(SYSSUB), PLCDEV=$(PLCDEV), PORT=$(PLCPORT), PLCIO=AI7, INOFF=74, OUTOFF=54, FLDDEV=$(TE), DESC='Beam In Bot.'")

#
#- MEBT Chopper Health Status
#
dbLoadRecords("mebtch_plc_do.template", "P=$(SYSSUB):, R=$(CHPDEV):, PLC=$(PLCPORT), INOFF=90, DESC='MEBT Chopper Health (to MPS)'")

#
#- PLC local protection system variables
#
dbLoadRecords("mebtch_plc_lps_vars.template", "P=$(SYSSUB):, R=$(PLCDEV):, PLC=$(PLCPORT), OUTOFF=78")


#
#---------------------------------- HV Pulsers Power Supply Control ----------------------------------
#
drvAsynIPPortConfigure("$(PULPOSDEV)","172.16.60.81:4001")
system "sleep 1" 
drvAsynIPPortConfigure("$(PULNEGDEV)","172.16.60.81:4002")
system "sleep 1"

#Give Asyn some time to successfully connect
dbLoadRecords("streamFIDPulser.template", "P=$(SYSSUB):, R=$(PULPOSDEV):, PULSCONN=$(PULPOSDEV)")
dbLoadRecords("streamFIDPulser.template", "P=$(SYSSUB):, R=$(PULNEGDEV):, PULSCONN=$(PULNEGDEV)")


#
#--------------------------- MEBT Chopper Operation/State Machine Control ----------------------------
#

#- Positive/Negative Pulser Voltage (Amplitude) setpoints and commands
epicsEnvSet("PULPOSVOL_OUT", "$(SYSSUB):$(PULPOSDEV):Vol-SP")
epicsEnvSet("PULNEGVOL_OUT", "$(SYSSUB):$(PULNEGDEV):Vol-SP")

#- Positive/Negative Pulser Pulse Duration (delay) setpoints and commands
epicsEnvSet("PULPOSDUR_OUT", "$(SYSSUB):$(PULPOSDEV):Dly-SP")
epicsEnvSet("PULNEGDUR_OUT", "$(SYSSUB):$(PULNEGDEV):Dly-SP")

#- Load records for the setpoint configuration of MEBT Chopper
dbLoadRecords("mebtch_pulsers.template", "P=$(SYSSUB):, R=$(CHPDEV):, PULPOSVOL_OUT=$(PULPOSVOL_OUT), PULNEGVOL_OUT=$(PULNEGVOL_OUT), PULPOSDUR_OUT=$(PULPOSDUR_OUT), PULNEGDUR_OUT=$(PULNEGDUR_OUT)")

#- Record (from PLC) that reflects overall HEALTH status (MPS output)
epicsEnvSet("MPS_HEALTH", "$(SYSSUB):$(CHPDEV):Health")

#- Vacuum status used to interlock in case of fault
epicsEnvSet("VAC_HEALTH", "MEBT-010:Vac-VGC-10000:Rly4_StatR")
#epicsEnvSet("VAC_HEALTH", "$(SYSSUB):$(CHPDEV):Vacuum-Sim")

#- Positive Pulser Health information to PLC
epicsEnvSet("PULPHEALTH_IN",  "$(SYSSUB):$(PULPOSDEV):Health")
epicsEnvSet("PULPHEALTH_OUT", "$(SYSSUB):$(PLCDEV):Var0-Stat-SP")

#- Negative Pulser Health information to PLC
epicsEnvSet("PULNHEALTH_IN", "$(SYSSUB):$(PULNEGDEV):Health")
epicsEnvSet("PULNHEALTH_OUT","$(SYSSUB):$(PLCDEV):Var1-Stat-SP")

#- Positive/Negative Pulser ON/OFF status
epicsEnvSet("PULPSTATUS_IN", "$(SYSSUB):$(PULPOSDEV):Status")
epicsEnvSet("PULNSTATUS_IN", "$(SYSSUB):$(PULNEGDEV):Status")

#- Positive/Negative Pulser ON/OFF commands
epicsEnvSet("PULPCMD_ON",  "$(SYSSUB):$(PULPOSDEV):OnCmd")
epicsEnvSet("PULPCMD_OFF", "$(SYSSUB):$(PULPOSDEV):OffCmd")
epicsEnvSet("PULNCMD_ON",  "$(SYSSUB):$(PULNEGDEV):OnCmd")
epicsEnvSet("PULNCMD_OFF", "$(SYSSUB):$(PULNEGDEV):OffCmd")

#- PLC variable to be written that indicates if state is HVON
epicsEnvSet("HVON_OUT", "$(SYSSUB):$(PLCDEV):Var2-Stat-SP")

#- EVR PVs (turn on/off triggers)
epicsEnvSet("EVRDEV", "$(SYSSUB):Ctrl-EVR-001")
epicsEnvSet("EVRDEV_OUT1","$(EVRDEV):Out-FPUV0-Src-SP")
epicsEnvSet("EVRDEV_OUT2","$(EVRDEV):Out-FPUV1-Src-SP")
epicsEnvSet("EVRDEV_SP","12")

#- Load main records
dbLoadRecords("mebtch_ctrl_statemac.template", "P=$(SYSSUB):, R=$(CHPDEV):, R_PLC=$(PLCDEV):, MPS_HEALTH=$(MPS_HEALTH), VAC_HEALTH=$(VAC_HEALTH), PULPHEALTH_IN=$(PULPHEALTH_IN), PULPHEALTH_OUT=$(PULPHEALTH_OUT), PULNHEALTH_IN=$(PULNHEALTH_IN), PULNHEALTH_OUT=$(PULNHEALTH_OUT), PULPSTATUS_IN=$(PULPSTATUS_IN), PULNSTATUS_IN=$(PULNSTATUS_IN), PULPCMD_ON=$(PULPCMD_ON), PULPCMD_OFF=$(PULPCMD_OFF), PULNCMD_ON=$(PULNCMD_ON), PULNCMD_OFF=$(PULNCMD_OFF), HVON_OUT=$(HVON_OUT), EVRDEV=$(EVRDEV), EVRDEV_OUT1=$(EVRDEV_OUT1), EVRDEV_OUT2=$(EVRDEV_OUT2), EVRDEV_SP=$(EVRDEV_SP)")

#- autosave configuration
epicsEnvSet("TOP", "/opt/nonvolatile-local")
epicsEnvSet("IOCDIR", "MEBT-010_Ctrl-IOC-001")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(TOP),IOCDIR=$(IOCDIR)")

#- asyn record
dbLoadRecords("asynRecord.db","P=$(SYSSUB):,R=$(CHPDEV):asynRecord1,PORT=$(PULPOSDEV), ADDR=0,OMAX=0,IMAX=0")
dbLoadRecords("asynRecord.db","P=$(SYSSUB):,R=$(CHPDEV):asynRecord2,PORT=$(PULNEGDEV), ADDR=0,OMAX=0,IMAX=0")

#- Start IOC
iocInit()

