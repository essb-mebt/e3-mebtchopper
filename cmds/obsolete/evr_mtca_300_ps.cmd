require mrfioc2,2.2.0-rc7
require mebt_fastchopper,master
require stream,2.8.8

#epicsEnvSet("ENGINEER","ESSBilbao") 
#epicsEnvSet("LOCATION","FEB-040Row") 
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")



#epicsEnvSet("EPICS_CAS_INTF_ADDR_LIST","172.16.60.78")
#epicsEnvSet("EPICS_CA_ADDR_LIST","172.16.60.78")

epicsEnvSet("SYS", "MEBT")
epicsEnvSet("SUB", "010")
epicsEnvSet("DIS", "TS")
epicsEnvSet("DEV", "EVR")
epicsEnvSet("IDX", "001")
epicsEnvSet("mTCA", "$(SYS)-$(SUB):$(DIS)-$(DEV)-$(IDX)")
epicsEnvSet("PSPOS","$(SYS)-$(SUB):PwrC-PSChop-001P")
epicsEnvSet("PSNEG","$(SYS)-$(SUB):PwrC-PSChop-001N")


# Not use in this script, but it is needed for the expansion. 
epicsEnvSet("MainEvtCODE" "14")

iocshLoad("$(mrfioc2_DIR)/evr-mtca-300.iocsh", "P=$(mTCA), PCIID=06:00.0")

# Loading delayModule on SLOT 1
dbLoadRecords("evr-delayModule-ess.db","EVR=$(DEV), P=$(mTCA),SLOT=1")
dbLoadRecords("chopper_evr.db","P=$(mTCA), SLOT=1")



# Connecting Positive and Negative PS
epicsEnvSet("STREAM_PROTOCOL_PATH","$(mebt_fastchopper_DB)")

drvAsynIPPortConfigure("myConnPos","127.0.0.1:9999")
#drvAsynIPPortConfigure("myConnNeg","127.0.0.1:1002")



#Give Asyn some time to successfully connect
#sleep 1

### LOAD RECORDS ###


dbLoadRecords("streamFIDPulser.db", "DEVICE=$(PSPOS), P=$(mTCA), Puls=Pos ,NUM=2")
#dbLoadRecords("streamFIDPulser.db", "DEVICE=$(PSNEG), P=$(mTCA), Puls=Neg ,NUM=3")




iocInit()





 



