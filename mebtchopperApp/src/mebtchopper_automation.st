program mebtchopper_automation_seq

%% #include <string.h>
%% #include <stdio.h>
%% #include <math.h>
%% #include <stdarg.h>

%{#include <stdlib.h>}%
%{#include <unistd.h>}%

option +d;

/* State machine info PV */
#define ERROR  0
#define STDBY  1
#define HVON   2


#define OK   1
#define NOK  0

#define ON   1
#define OFF  0


/********************* EPICS PVs declaration *************************/

/* State machine command PV */
int state_sp;
assign state_sp to "{STATEMACHINESP}";
monitor state_sp;
evflag commandFlag;
sync state_sp commandFlag;

/* State machine readback PV */
int state_rb;
assign state_rb to "{STATEMACHINERB}";

/* Health status from PLC */
int chopper_health;
assign chopper_health to "{CHOPPERHEALTHRB}";
monitor chopper_health;

/* HVON setpoint to PLC */
int hvon_plc;
assign hvon_plc to "{HVONPLC}";

/********************* Sequencer State Machine *************************/

ss mebtchopper_automation_ss {


	/**************************** ERROR STATE *******************************************/
	/* Turn-off pulsers, wait for chopper health to be OK                               */
	/*                                                                                  */ 
	/************************************************************************************/
	
	state error_state {

		entry {
			state_rb = ERROR;
			pvPut(state_rb);

            hvon_plc = OFF;
            pvPut(hvon_plc, SYNC);

            pvGet(chopper_health);
            printf("[SEQUENCER] Entering error_state\n");
		}

    	when (chopper_health == OK) {}
		state stdby_state

		when(delay(1)) {}
		state error_state
	}


	/**************************** STDBY STATE *******************************************/
	/* Chopper health is OK, wait for user commeand to start pulsers (HVON)             */
	/* if chopper health is NOK, go back to ERROR                                       */ 
	/************************************************************************************/

    state stdby_state {
		
        entry {
			state_rb = STDBY;
			pvPut(state_rb);

            hvon_plc = OFF;
            pvPut(hvon_plc, SYNC);

            pvGet(chopper_health);
            printf("[SEQUENCER] Entering stdby_state\n");
		}

    	when (chopper_health == NOK) {}
		state error_state

        // Detect when user sends command 
   		when ((state_sp == HVON)  && efTestAndClear(commandFlag)) {}
		state hvon_state

		when(delay(1)) {}
		state stdby_state
    }


	/***************************** HVON STATE *******************************************/
	/* Turn ON the pulsers                                                              */
	/* go to error if there is any problem with Chopper Health                          */ 
	/************************************************************************************/

    state hvon_state {

		entry {
			state_rb = HVON;
			pvPut(state_rb);

            hvon_plc = ON;
            pvPut(hvon_plc, SYNC);

            pvGet(chopper_health);
            printf("[SEQUENCER] Entering hvon_state\n");
		}

    	when (chopper_health == NOK) {}
		state error_state

        // Detect when user sends command 
   		when ((state_sp == STDBY)  && efTestAndClear(commandFlag)) {}
		state stdby_state

		when(delay(1)) {}
		state hvon_state
    }

} // end of state set (ss)
