# List of PLC Signals
===

### Module 1 [F] - DI 8x24VDC HF
DI0 -> Load + Water Flow
DI1 -> Load - Water Flow
DI2 ->
DI3 ->
DI4 -> MEBT Dump Water Flow
DI5 ->
DI6 ->
DI7 ->

### Module 2 [G] - DI 8x24VDC HF
DI8  -> 24V MOdule OK
DI9  ->
DI10 ->
DI11 ->
DI12 ->
DI13 ->
DI14 ->
DI15 ->

### Module 3 [H] - DQ 8x24VDC/0.5A ST
DO0 -> Health Signal
DO1 -> HV ON (to WS)
DO2 -> HV ON (to WS)
DO3 ->
DO4 -> 
DO5 ->
DO6 ->
DO7 ->

### Module 4 [J] - AI 4xRTD/TC 2,3,4-wire HF
AI0 -> Load + Water Temp. (TE1)
AI1 -> Load - Water Temp. (TE2)
AI2 -> Dump Water Temp. (TE7)
AI3 ->

### Module 5 [K] - AI 4xRTD/TC 2,3,4-wire HF
AI4 -> Beam Out Bottom (TE3)
AI5 -> Beam Out Top (TE4)
AI6 -> Beam In Top (TE5)
AI7 -> Beam In Bottom (TE6)

