MEBT Chopper EPICS Support
======

European Spallation Source ERIC Site-specific EPICS module : e3-mebtchopper 

Summary 
================ 
This module provides EPICS support for the FID Technology HV Fast Pulser Power Supply (custom design for ESS) and also for the MEBT Chopper Local Protection PLC (Siemens S7-1500 Family).


Dependencies 
============ 
* s7plc
* streamDevice

Using the module 
================ 
Load the module and dependencies:

     require mebtchopper
     require s7plc
     require stream

Then load the main modules for the PLC and/or Pulsers control:

     TBD

Building the module 
=================== 
This module is structured as an E3 "local module", meaning that it doesn't rely on submodules from elsewhere.

* Edit your E3 configurations in the `configure/RELEASE` and `configure/CONFIG_MODULE` files;
* Run `make init` and `make rebuild` from the command line to install the module into your E3 environment;
