# FID pulser simulator

Note it depends on kameleon (https://bitbucket.org/europeanspallationsource/kameleon/src/master)

# Auxiliary tools

Useful online CRC calculator: https://crccalc.com
* Output type:  HEX
* Calc:         CRC-16
* Algorithm:    CRC-16/MODBUS

